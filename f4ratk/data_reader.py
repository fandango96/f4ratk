##############################################################################
# Copyright (C) 2020 - 2022 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from bs4 import BeautifulSoup
from datetime import date, datetime, timedelta
from os import PathLike
from typing import Optional

from pandas import DataFrame, Series, concat, date_range, read_csv, read_html, to_datetime
from pandas_datareader.famafrench import FamaFrenchReader
from pandas_datareader.fred import FredReader
from pandas_datareader.yahoo.daily import YahooDailyReader
from requests_cache import CachedSession

from f4ratk.directories import cache
from f4ratk.domain import Currency

import json
import math


def _cached_session() -> CachedSession:
    cache_duration = timedelta(days=14)
    cache_location = str(cache.file(name='requests'))

    session = CachedSession(
        cache_name=cache_location, backend='sqlite', expire_after=cache_duration
    )
    session.remove_expired_responses()

    session.headers.update(
        {
            # Workaround, see https://github.com/pydata/pandas-datareader/issues/867
            'User-Agent': (
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64)'
                ' AppleWebKit/537.36 (KHTML, like Gecko)'
                ' Chrome/91.0.4472.124 Safari/537.36'
            )
        }
    )

    return session


_session = _cached_session()


def fama_french_reader(returns_data: str) -> FamaFrenchReader:
    return FamaFrenchReader(symbols=returns_data, session=_session, start='1920')


def yahoo_reader(
    ticker_symbol: str, start: Optional[date], end: Optional[date]
) -> YahooDailyReader:
    return YahooDailyReader(
        symbols=ticker_symbol,
        start=start if start else '1970',
        end=end,
        session=_session,
    )


def fred_reader(
    exchange_symbol: str, start: Optional[date], end: Optional[date]
) -> FredReader:
    return FredReader(
        symbols=exchange_symbol,
        start=start if start else '1970',
        end=end,
        session=_session,
    )


class CsvFileReader:
    _HEADER = ('Dates', 'Returns')

    def __init__(self, path: PathLike):
        self._path = path

    def read(self) -> DataFrame:
        return read_csv(self._path, parse_dates=True, index_col=0, names=self._HEADER)


class FtReader:
    def __init__(self, isin: str, currency: Currency, start: Optional[date], end: Optional[date]):
        self._isin = isin
        self._currency = currency
        self._start = start if start else date(1970, 1, 1)
        self._end = end if end else date.today()

    def format_date(self, date):
        date = date.split(',')[-2][1:] + date.split(',')[-1]
        return Series({'Date': date})

    def update_start(self):
        url = f"https://markets.ft.com/data/funds/tearsheet/summary?s={self._isin}:{self._currency.name}"
        response = _session.get(url)
        soup = BeautifulSoup(response.content, "html.parser")
        launch_date = datetime.strptime(soup.find('th', string='Launch date').find_next_sibling('td').string, '%d %b %Y').date()
        if launch_date > self._start:
            self._start = launch_date

    def read(self) -> DataFrame:
        self.update_start()
        periods = math.ceil((self._end.year - self._start.year) / 10) + 1
        datelist = date_range(start=self._start, end=self._end, periods=periods)
        df = DataFrame(None, columns=['Date', 'Open', 'High', 'Low', 'Close', 'Volume'])
        url = f"https://markets.ft.com/data/funds/tearsheet/historical?s={self._isin}:{self._currency.name}"
        response = _session.get(url)
        soup = BeautifulSoup(response.content, "html.parser")
        elemList = soup.find_all('section', {'class':'mod-tearsheet-add-to-watchlist'})
        for elem in elemList:
            elemID = elem.get('class')
            elemName = elem.get('data-mod-config')
            if elemName is None:
                pass
            elif 'xid' in elemName:
                data = json.loads(elemName)
                val1 = data['xid']
                for start_date, end_date in zip(datelist, datelist[1:]):
                    if start_date != self._start:
                        start_date = start_date + timedelta(days=1)
                    start = start_date.strftime('%Y/%m/%d')
                    end = end_date.strftime('%Y/%m/%d')
                    r = _session.get(f'https://markets.ft.com/data/equities/ajax/get-historical-prices?startDate={start}&endDate={end}&symbol={val1}').json()
                    df_temp = read_html('<table>'+r['html']+'</table>')[0]
                    df_temp.columns=['Date','Open','High','Low','Close','Volume']
                    df = concat([df, df_temp], ignore_index=True)
                df['Adj Close'] = df['Close']
                df['Date'] = df['Date'].apply(self.format_date)
                df['Date'] = to_datetime(df['Date'], format='%b %d %Y')
                df.set_index('Date', inplace=True)
                return df[df.index.dayofweek < 5]
