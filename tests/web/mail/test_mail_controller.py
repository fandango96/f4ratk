##############################################################################
# Copyright (C) 2020 - 2022 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from email.message import EmailMessage
from smtplib import SMTP
from typing import Generator
from unittest.mock import Mock

from flask import Response
from flask.testing import FlaskClient
from pytest import fixture
from pytest_mock import MockerFixture


@fixture(scope='function')
def mail_server_config(create_env) -> None:
    create_env(
        {
            'MAIL_FROM_ADDRESS': "from@example.org",
            'MAIL_TO_ADDRESS': "to@example.org",
            'MAIL_SERVER_USERNAME': "server-login",
            'MAIL_SERVER_PASSWORD': "1234",
            'MAIL_SERVER_HOST': "mail.example.org",
        }
    )


@fixture(scope='function')
def smtp_server(mocker: MockerFixture) -> Generator[Mock, None, None]:

    connection = Mock(spec_set=SMTP)

    mocker.patch(
        'f4ratk.web.mail.mail.SMTP',
        spec_set=SMTP,
        **{'__enter__.return_value': connection}
    )

    yield connection


def given_only_visible_fields_set_when_mail_request_received_should_send_regular_mail(
    mail_server_config, smtp_server: Mock, client: FlaskClient
):
    body = {
        "name": "John Doe",
        "contact": "john.doe@example.org",
        "content": "Some Feedback",
    }

    response: Response = client.post(
        "/v0/mails", json=body, content_type='application/json'
    )

    assert response.status_code == 204
    assert response.headers["Content-Type"] == "application/json"
    assert response.data == b''

    smtp_server.starttls.assert_called_once()
    smtp_server.login.assert_called_once_with(user='server-login', password='1234')

    class SpecificEmailMessage:
        def __eq__(self, other: EmailMessage):
            assert type(other) is EmailMessage
            assert "[f4ratk]" in other['Subject']
            assert "john.doe@example.org" in other['Subject']
            assert "John Doe" in other['Subject']
            assert "[BOT]" not in other['Subject']
            assert other['To'] == "to@example.org"
            assert other['From'] == "from@example.org"
            assert "Some Feedback" in other.get_content()
            return True

    smtp_server.send_message.assert_called_once_with(SpecificEmailMessage())

    smtp_server.quit.assert_called_once()


def given_invisible_fields_set_when_mail_request_received_should_send_mail_and_mark_as_bot(  # noqa: E501
    mail_server_config, smtp_server: Mock, client: FlaskClient
):
    body = {"address": "BotAddress", "additionalName": "BotName"}

    response: Response = client.post(
        "/v0/mails", json=body, content_type='application/json'
    )

    assert response.status_code == 204
    assert response.headers["Content-Type"] == "application/json"
    assert response.data == b''

    smtp_server.starttls.assert_called_once()
    smtp_server.login.assert_called_once_with(user='server-login', password='1234')

    class SpecificEmailMessage:
        def __eq__(self, other: EmailMessage):
            assert type(other) is EmailMessage
            assert "[BOT]" in other['Subject']
            return True

    smtp_server.send_message.assert_called_once_with(SpecificEmailMessage())

    smtp_server.quit.assert_called_once()


def given_invalid_contact_when_mail_request_received_should_respond_error(
    mail_server_config,
    smtp_server: Mock,
    client: FlaskClient,
):
    body = {
        "contact": "example.org",
    }

    response: Response = client.post(
        "/v0/mails", json=body, content_type='application/json'
    )

    assert response.status_code == 400
    assert response.headers["Content-Type"] == "application/json"

    body = response.get_json()

    assert body['errors'][0]['code'] == "INVALID"
    assert body['errors'][0]['status'] == "400"
    assert body['errors'][0]['title'] == 'Invalid Attribute'
    assert body['errors'][0]['source']['pointer'] == '/contact'
    assert body['errors'][0]['detail'] == 'Not a valid email address.'

    smtp_server.login.assert_not_called()
    smtp_server.send_message.assert_not_called()
