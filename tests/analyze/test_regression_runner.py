##############################################################################
# Copyright (C) 2020 - 2022 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from f4ratk.analyze.regression import RegressionRunner
from tests.conftest import FamaFactory, ReturnsFactory


def given_any_data_when_run_should_calculate_robust_std_errors_via_neweywest_and_default_lag_length(  # noqa: E501
    create_fama: FamaFactory, create_returns: ReturnsFactory
):
    fama = create_fama(
        ('2021-01', 5),
    )

    quotes = create_returns(
        ('2021-01', 5),
    )

    results = RegressionRunner().run(returns=quotes, fama_data=fama)

    assert results.ff6.model.cov_kwds['maxlags'] is None
