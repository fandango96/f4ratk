##############################################################################
# Copyright (C) 2020 - 2022 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from datetime import date
from pathlib import Path
from typing import Generator
from unittest.mock import ANY, Mock

from _pytest.pytester import LineMatcher
from click.testing import CliRunner
from pytest import fixture, mark
from pytest_mock import MockerFixture

from f4ratk.cli.commands import main
from f4ratk.domain import AnalysisConfig, Currency, Frame, Frequency, Region
from f4ratk.file.api import FileAnalyzer
from f4ratk.file.reader import FileConfig, ValueFormat


class TestFileCommandHelp:
    @fixture(scope='class', name='output')
    def given_help_option_when_invoked(self) -> Generator[str, None, None]:
        result = CliRunner().invoke(main, ('file', '--help'))
        assert result.exit_code == 0
        yield result.output

    def should_display_help_option(self, output: str):
        assert '--help' in output

    def should_display_file_path_argument(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(r"Usage\:.*?file.*?" + "PATH")

    def should_display_region_argument(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            r"Usage\:.*?file.*?" + r"\{DEVELOPED\|DEVELOPED-EX-US\|US\|EU\|EMERGING\}"
        )

    def should_display_currency_argument(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            (r"Usage\:.*?file.*?", r"\s+\{USD\|EUR\}"), consecutive=True
        )

    def should_display_value_format_argument(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            (r"Usage\:.*?file.*?", r".*?\{PRICE\|RETURN\}"), consecutive=True
        )

    def should_display_start_date_option(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            '.*?'.join(
                (
                    '',
                    r'\-\-start',
                    r'\[\%Y\-\%m\-\%d\]',
                    r'Start\ of\ period\ under\ review\.',
                )
            )
        )

    def should_display_end_date_option(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            '.*?'.join(
                (
                    '',
                    r'\-\-end',
                    r'\[\%Y\-\%m\-\%d\]',
                    r'End\ of\ period\ under\ review\.',
                )
            )
        )

    def should_display_frequency_option(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            (
                '.*?'.join(
                    (
                        '',
                        r'\-\-frequency',
                        r'\[DAILY|MONTHLY]',
                        'Conduct analysis with given sample frequency.',
                    )
                ),
                '.*?'.join(('', r'\[default\:\ MONTHLY\]')),
            )
        )


class TestFileCommand:
    @fixture(scope='function', name='mock')
    def mock_analyze_file(self, mocker: MockerFixture) -> Generator[Mock, None, None]:
        mock = mocker.patch(
            'f4ratk.infrastructure.FileAnalyzerAdapter.analyze_file',
            spec_set=FileAnalyzer,
        )
        yield mock

    def given_any_path_when_analyzing_should_pass_same_path(self, mock: Mock):
        CliRunner().invoke(main, ('file', '/nonexistent.csv', 'US', 'USD', 'PRICE'))

        mock.assert_called_once_with(
            file_config=FileConfig(
                path=Path('/nonexistent.csv'),
                currency=ANY,
                value_format=ANY,
            ),
            analysis_config=ANY,
        )

    def given_any_start_date_in_iso_when_analyzing_should_pass_start_date(
        self, mock: Mock
    ):
        CliRunner().invoke(
            main,
            ('file', '/nonexistent.csv', 'US', 'USD', 'PRICE', '--start=2020-08-01'),
        )

        mock.assert_called_once_with(
            file_config=FileConfig(path=ANY, currency=ANY, value_format=ANY),
            analysis_config=AnalysisConfig(
                region=ANY, frame=Frame(frequency=ANY, start=date(2020, 8, 1), end=None)
            ),
        )

    def given_any_end_date_in_iso_when_analyzing_should_pass_end_date(self, mock: Mock):
        CliRunner().invoke(
            main, ('file', '/nonexistent.csv', 'US', 'USD', 'PRICE', '--end=2020-08-31')
        )

        mock.assert_called_once_with(
            file_config=FileConfig(path=ANY, currency=ANY, value_format=ANY),
            analysis_config=AnalysisConfig(
                region=ANY,
                frame=Frame(frequency=ANY, start=None, end=date(2020, 8, 31)),
            ),
        )

    @mark.parametrize(
        'region_arg, expected_region',
        (
            ('DEVELOPED', Region.DEVELOPED),
            ('developed', Region.DEVELOPED),
            ('Developed', Region.DEVELOPED),
            ('DEVELOPED-EX-US', Region.DEVELOPED_EX_US),
            ('developed-ex-us', Region.DEVELOPED_EX_US),
            ('Developed-Ex-Us', Region.DEVELOPED_EX_US),
            ('EMERGING', Region.EMERGING),
            ('emerging', Region.EMERGING),
            ('Emerging', Region.EMERGING),
            ('US', Region.US),
            ('us', Region.US),
            ('uS', Region.US),
            ('EU', Region.EU),
            ('eu', Region.EU),
            ('Eu', Region.EU),
        ),
    )
    def given_any_valid_region_case_insensitive_when_analyzing_should_pass_region_object(  # noqa: E501
        self, mock: Mock, region_arg: str, expected_region: Region
    ):
        CliRunner().invoke(
            main, ('file', '/nonexistent.csv', region_arg, 'USD', 'PRICE')
        )

        mock.assert_called_once_with(
            file_config=FileConfig(path=ANY, currency=ANY, value_format=ANY),
            analysis_config=AnalysisConfig(region=expected_region, frame=ANY),
        )

    @mark.parametrize(
        'currency_arg, expected_currency',
        (
            ('USD', Currency.USD),
            ('usd', Currency.USD),
            ('uSd', Currency.USD),
            ('EUR', Currency.EUR),
            ('eur', Currency.EUR),
            ('Eur', Currency.EUR),
        ),
    )
    def given_any_valid_currency_case_insensitive_when_analyzing_should_pass_currency_object(  # noqa: E501
        self, mock: Mock, currency_arg: str, expected_currency: Currency
    ):
        CliRunner().invoke(
            main, ('file', '/nonexistent.csv', 'US', currency_arg, 'PRICE')
        )

        mock.assert_called_once_with(
            file_config=FileConfig(
                path=ANY, currency=expected_currency, value_format=ANY
            ),
            analysis_config=ANY,
        )

    @mark.parametrize(
        'value_format_arg, expected_format',
        (
            ('PRICE', ValueFormat.PRICE),
            ('price', ValueFormat.PRICE),
            ('Price', ValueFormat.PRICE),
            ('RETURN', ValueFormat.RETURN),
            ('return', ValueFormat.RETURN),
            ('Return', ValueFormat.RETURN),
        ),
    )
    def given_any_valid_value_format_case_insensitive_when_analyzing_should_pass_value_format_object(  # noqa: E501
        self, mock: Mock, value_format_arg: str, expected_format: ValueFormat
    ):
        CliRunner().invoke(
            main, ('file', '/nonexistent.csv', 'US', 'USD', value_format_arg)
        )

        mock.assert_called_once_with(
            file_config=FileConfig(
                path=ANY, currency=ANY, value_format=expected_format
            ),
            analysis_config=ANY,
        )

    @mark.parametrize(
        'frequency_arg, expected_frequency',
        (
            ('DAILY', Frequency.DAILY),
            ('daily', Frequency.DAILY),
            ('DailY', Frequency.DAILY),
            ('MONTHLY', Frequency.MONTHLY),
            ('monthly', Frequency.MONTHLY),
            ('MONTHLY', Frequency.MONTHLY),
        ),
    )
    def given_any_valid_frequency_case_insensitive_when_analyzing_should_pass_frequency_object(  # noqa: E501
        self, mock: Mock, frequency_arg: str, expected_frequency: Frequency
    ):
        CliRunner().invoke(
            main,
            (
                'file',
                '/nonexistent.csv',
                'US',
                'USD',
                'RETURN',
                f"--frequency={frequency_arg}",
            ),
        )

        mock.assert_called_once_with(
            file_config=ANY,
            analysis_config=AnalysisConfig(
                region=ANY,
                frame=Frame(frequency=expected_frequency, start=ANY, end=ANY),
            ),
        )
