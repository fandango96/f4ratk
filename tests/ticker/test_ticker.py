##############################################################################
# Copyright (C) 2020 - 2022 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from unittest.mock import Mock

from pandas import DatetimeIndex, PeriodDtype, PeriodIndex
from pandas.tseries.offsets import BusinessDay
from pandas_datareader._utils import RemoteDataError
from pytest import approx, mark, raises
from pytest_mock import MockerFixture

from f4ratk.domain import Currency, Frequency
from f4ratk.shared import Downsampler, Normalizer
from f4ratk.ticker.api import NoTickerData
from f4ratk.ticker.reader import Stock, TickerDataReader
from tests.conftest import QuotesFactory


class TestTickerReader:
    def given_any_ticker_symbol_when_reading_stock_data_should_return_data_with_business_daily_frequency(  # noqa: E501
        self,
    ):
        returns = TickerDataReader(
            exchange_reader=None, normalizer=Normalizer(), downsampler=None
        ).data(
            stock=Stock(ticker_symbol='EUNL.DE', currency=Currency.USD),
            frequency=Frequency.DAILY,
            start=None,
            end=None,
        )

        assert isinstance(returns.index, PeriodIndex)
        assert returns.index.dtype == PeriodDtype(freq=BusinessDay())

    def given_any_ticker_symbol_when_reading_stock_data_should_return_only_returns_column(  # noqa: E501
        self,
    ):
        returns = TickerDataReader(
            exchange_reader=None, normalizer=Normalizer(), downsampler=None
        ).data(
            stock=Stock(ticker_symbol='EUNL.DE', currency=Currency.USD),
            frequency=Frequency.DAILY,
            start=None,
            end=None,
        )

        assert list(returns.columns) == ['Returns']

    def given_any_ticker_symbol_when_reading_stock_data_should_convert_adjusted_close_to_returns_as_relative_percentage(  # noqa: E501
        self, mocker: MockerFixture, create_quotes: QuotesFactory
    ):
        def given():
            mock_reader = Mock()
            mock_reader.read.return_value = create_quotes(
                ('2020-04-01', 3.0),
                ('2020-04-02', 1.5),
                ('2020-04-03', 2.0),
                index=DatetimeIndex,
            )
            mocker.patch('f4ratk.ticker.reader.yahoo_reader', return_value=mock_reader)

        given()

        returns = TickerDataReader(
            exchange_reader=None, normalizer=Normalizer(), downsampler=None
        ).data(
            stock=Stock(ticker_symbol='EUNL.DE', currency=Currency.USD),
            frequency=Frequency.DAILY,
            start=None,
            end=None,
        )

        assert returns['Returns']['2020-04-02'] == -50.0
        assert returns['Returns']['2020-04-03'] == approx(33.33333, rel=0.0001)

    def given_monthly_frequency_and_daily_data_when_reading_ticker_symbol_should_interpolate_if_month_end_missing_and_resample_to_monthly(  # noqa: E501
        self, mocker: MockerFixture, create_quotes: QuotesFactory
    ):
        def given():
            mock_reader = Mock()
            mock_reader.read.return_value = create_quotes(
                ('2020-03-31', 3.75),
                ('2020-04-27', 3.0),
                ('2020-05-01', 1.5),
                ('2020-05-29', 2.0),
                index=DatetimeIndex,
            )
            mocker.patch('f4ratk.ticker.reader.yahoo_reader', return_value=mock_reader)

        given()

        returns = TickerDataReader(
            exchange_reader=None, normalizer=Normalizer(), downsampler=Downsampler()
        ).data(
            stock=Stock(ticker_symbol='EUNL.DE', currency=Currency.USD),
            frequency=Frequency.MONTHLY,
            start=None,
            end=None,
        )

        assert returns['Returns']['2020-04'] == -50.0
        assert returns['Returns']['2020-05'] == approx(6.66666, rel=0.0001)

    @mark.parametrize("reader_error", [RemoteDataError(), KeyError()])
    def given_unknown_symbol_when_reading_ticker_symbol_should_raise_no_ticker_data_error(  # noqa: E501
        self, mocker: MockerFixture, reader_error: Exception
    ):
        mocker.patch('f4ratk.ticker.reader.yahoo_reader', side_effect=reader_error)

        with raises(NoTickerData):
            TickerDataReader(
                exchange_reader=None, normalizer=None, downsampler=None
            ).data(
                stock=Stock(ticker_symbol='UNKNOWN', currency=Currency.USD),
                frequency=Frequency.MONTHLY,
                start=None,
                end=None,
            )
