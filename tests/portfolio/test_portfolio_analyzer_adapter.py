from datetime import date
from typing import Optional
from unittest.mock import ANY, Mock

from pandas import DataFrame

from f4ratk.analyze.api import DataAnalyzer
from f4ratk.domain import AnalysisConfig, Frame, Frequency, Region
from f4ratk.fama import FamaReader
from f4ratk.file.api import FileReader
from f4ratk.file.reader import FileConfig
from f4ratk.history import History
from f4ratk.portfolio.analyze import PortfolioAnalyzerAdapter
from f4ratk.portfolio.ports import (
    PortfolioConfiguration,
    PortfolioReader,
    PortfolioRequest,
    Source,
)
from f4ratk.ticker.api import TickerReader
from f4ratk.ticker.reader import Stock
from tests.conftest import QuotesFactory


def given_portfolio_with_file_and_stock_when_analyzing_then_should_read_and_combine_returns_and_pass_to_analysis(  # noqa: E501
    create_quotes: QuotesFactory,
):
    config = PortfolioConfiguration(
        sources=(
            Source(Stock(ticker_symbol=None, currency=None), weight=25),
            Source(FileConfig(path=None, currency=None, value_format=None), weight=75),
        ),
        config=AnalysisConfig(
            region=Region.EMERGING,
            frame=Frame(
                frequency=Frequency.DAILY, start=date(2001, 1, 1), end=date(2001, 6, 30)
            ),
        ),
    )

    fama_data = DataFrame()
    historic_data = DataFrame()

    stock_data = create_quotes(('2021-01', 5), ('2021-02', 10), ('2021-03', 20))
    file_data = create_quotes(('2021-01', 10), ('2021-02', 1))

    portfolio_reader = Mock(spec_set=PortfolioReader, **{'read.return_value': config})
    fama_reader = Mock(spec_set=FamaReader, **{'fama_data.return_value': fama_data})
    history = Mock(
        spec_set=History, **{'annualized_returns.return_value': historic_data}
    )
    analyzer = Mock(spec_set=DataAnalyzer)

    def ticker_reader_stub(stock: Stock, frame: Frame) -> Optional[DataFrame]:
        return stock_data if isinstance(stock, Stock) else None

    def file_reader_stub(file_config: FileConfig, frame: Frame) -> Optional[DataFrame]:
        return file_data if isinstance(file_config, FileConfig) else None

    PortfolioAnalyzerAdapter(
        portfolio_reader=portfolio_reader,
        fama_reader=fama_reader,
        ticker_reader=Mock(
            spec_set=TickerReader, **{'read_ticker.side_effect': ticker_reader_stub}
        ),
        file_reader=Mock(spec_set=FileReader, **{'read.side_effect': file_reader_stub}),
        analyzer=analyzer,
        history=history,
    ).analyze_portfolio_file(request=PortfolioRequest(path=None, name=None))

    portfolio_reader.read.assert_called_once_with(
        request=PortfolioRequest(path=None, name=None)
    )

    analyzer.analyze.assert_called_once_with(ANY, fama_data, historic_data)
    assert analyzer.analyze.call_args[0][0].equals(
        create_quotes(('2021-01', 8.75), ('2021-02', 3.25))
    )
