##############################################################################
# Copyright (C) 2020 - 2022 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from datetime import date
from unittest.mock import Mock

from pandas import DatetimeIndex
from pytest import fixture

from f4ratk.data_reader import CsvFileReader
from f4ratk.domain import Currency, Frequency
from f4ratk.file.reader import FileContentReader, FileReader, ValueFormat
from f4ratk.shared import Normalizer
from tests.conftest import Quote, ReturnsFactory


@fixture
def given_file_reader_and_daily_price_data(
    create_returns: ReturnsFactory,
) -> FileReader:
    def _given_daily_price_data(
        *quotes: Quote,
    ):
        data = create_returns(
            *quotes,
            index=DatetimeIndex,
        )

        mock_reader = Mock(spec_set=CsvFileReader, read=Mock(return_value=data))

        return FileContentReader(
            csv_reader=mock_reader,
            exchange_reader=None,
            currency=Currency.USD,
            value_format=ValueFormat.PRICE,
            normalizer=Normalizer(),
        )

    return _given_daily_price_data


def given_daily_price_data_when_reading_with_start_date_should_limit_results_to_inclusive_start_onwards(  # noqa: E501
    given_file_reader_and_daily_price_data,
):
    result = given_file_reader_and_daily_price_data(
        ('2021-03-08', 1.5),
        ('2021-03-09', 2.0),
        ('2021-03-10', 3.0),
        ('2021-03-11', 4.5),
    ).read(start=date(2021, 3, 9), end=None, frequency=Frequency.DAILY)

    assert [str(idx) for idx in result.index] == ['2021-03-10', '2021-03-11']


def given_daily_price_data_when_reading_with_end_date_should_limit_results_up_to_inclusive_end(  # noqa: E501
    given_file_reader_and_daily_price_data,
):
    result = given_file_reader_and_daily_price_data(
        ('2021-03-08', 1.5),
        ('2021-03-09', 2.0),
        ('2021-03-10', 3.0),
        ('2021-03-11', 4.5),
    ).read(start=None, end=date(2021, 3, 10), frequency=Frequency.DAILY)

    assert [str(idx) for idx in result.index] == ['2021-03-09', '2021-03-10']


def given_daily_price_data_when_reading_with_start_and_end_date_should_limit_results_to_inclusive_period(  # noqa: E501
    given_file_reader_and_daily_price_data,
):
    result = given_file_reader_and_daily_price_data(
        ('2021-03-08', 1.5),
        ('2021-03-09', 2.0),
        ('2021-03-10', 3.0),
        ('2021-03-11', 4.5),
    ).read(start=date(2021, 3, 9), end=date(2021, 3, 10), frequency=Frequency.DAILY)

    assert len(result) == 1
    assert result['Returns']['2021-03-10'] == 50.0
